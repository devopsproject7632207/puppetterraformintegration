provider "aws" {
  region = "us-east-1"
}

resource "aws_instance" "example" {
  count         = 2
  ami           = "ami-07d9b9ddc6cd8dd30"
  instance_type = "t2.micro"
  key_name      = "terraform-key"
  security_groups = ["puppet"]

  tags = {
    Name = "example-instance-${count.index + 1}"
  }
}
