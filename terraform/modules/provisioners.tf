provider "aws" {
  region = "us-east-1"  
}

resource "null_resource" "remote_exec" {
  connection {
    type        = "ssh"
    host        = "54.152.105.217"  
    user        = "ubuntu"  
    private_key = file("C:/Users/HP/Desktop/terraform-key.pem")  

  provisioner "remote-exec" {
    inline = [
      "sudo /opt/puppetlabs/bin/puppet agent --test"
    ]
  }
}
