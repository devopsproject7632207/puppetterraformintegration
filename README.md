# PuppetTerraformIntegration



## Objective

The objective of this assignment is to automate the configuration management of
infrastructure resources provisioned using Terraform using Puppet

## Infrastructure Provisioning with Terraform:

I have created two aws instances named as example-instance-1 and example-instance-2 using terraform which uses existing security group named as "puppet" as well as it uses a key-pair named as "terraform-key". The code to create the instances is written in main.tf file in the terraform directory. I have renamed the instances as "puppet-master" and "puppet-slave".

## Puppet installation

I have installed the puppet and setup the master-slave architechture on the created aws instances. The puppet master and puppet agent are interacting efficiently.

## Installation of apache on master instance

The apache software is installed on the master node sucessfully. 

## Write puppet manifest files

i have wrote manifest file named as "site.pp" which contains:
    - Installing required packages or software.  
    - Configuring system settings or files.  
    - Starting or enabling services.
To install the apache properly i had to create "site.conf.erb" file as a template for site.pp file.

## Using terraform provisioners

I connected to the puppet-slave instance using terraform provisioners and to trigger the puppet agent to make infrastructural changes according to the changes in the puppet master manifest files. this file is stored inside the "terrform/modules" directory named as "provisioners.tf".

## Terraform provisioners sucess

The terrafom provisioners connect to the puppet agent instance successfully and run the commands thoroughly. 
We can also see the apache2 output on the aws instance as well as web browser using the public ip address.

## Documentation

I did proper Documentation for the steps I did for the assignment.
Even i shared the necessary screenshots in the "images" folder

 
 
