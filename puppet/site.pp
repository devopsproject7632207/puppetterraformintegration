#  Install Apache HTTP Server and PHP packages on Puppet agent nodes
package { ['apache2', 'php']:
  ensure => installed,
}

# Configure Apache HTTP Server
file { '/etc/apache2/sites-available/your_site.conf':
  ensure  => file,
  content => template('apache/site.conf.erb'),
  notify  => Service['apache2'],
}

# Start and enable Apache HTTP Server
service { 'apache2':
  ensure  => running,
  enable  => true,
  require => Package['apache2'],
}
